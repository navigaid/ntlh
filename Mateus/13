Mateus 13
1
Naquele mesmo dia Jesus saiu de casa, foi para a beira do lago da Galiléia, sentou-se ali e começou a ensinar.
2
A multidão que se ajuntou em volta dele era tão grande, que ele entrou num barco e sentou-se; e o povo ficou em pé na praia.
3
Jesus usou parábolas para ensinar muitas coisas. Ele disse: —Escutem! Certo homem saiu para semear.
4
Quando estava espalhando as sementes, algumas caíram na beira do caminho, e os passarinhos comeram tudo.
5
Outra parte das sementes caiu num lugar onde havia muitas pedras e pouca terra. As sementes brotaram logo porque a terra não era funda.
6
Mas, quando o sol apareceu, queimou as plantas, e elas secaram porque não tinham raízes.
7
Outras sementes caíram no meio de espinhos, que cresceram e sufocaram as plantas.
8
Mas as sementes que caíram em terra boa produziram na base de cem, de sessenta e de trinta grãos por um.
9
E Jesus terminou, dizendo: —Se vocês têm ouvidos para ouvir, então ouçam.
10
Então os discípulos chegaram perto de Jesus e perguntaram: —Por que é que o senhor usa parábolas para falar com essas pessoas?
11
Jesus respondeu: —A vocês Deus mostra os segredos do Reino do Céu, mas, a elas, não.
12
Pois quem tem receberá mais, para que tenha mais ainda. Mas quem não tem, até o pouco que tem lhe será tirado.
13
É por isso que eu uso parábolas para falar com essas pessoas. Porque elas olham e não enxergam; escutam e não ouvem, nem entendem.
14
E assim acontece com essas pessoas o que disse o profeta Isaías: “Vocês ouvirão, mas não entenderão; olharão, mas não enxergarão nada.
15
Pois a mente deste povo está fechada: Eles taparam os ouvidos e fecharam os olhos. Se eles não tivessem feito isso, os seus olhos poderiam ver, e os seus ouvidos poderiam ouvir; a sua mente poderia entender, e eles voltariam para mim, e eu os curaria! —disse Deus. ”
16
Jesus continuou, dizendo: —Mas vocês, como são felizes! Pois os seus olhos vêem, e os seus ouvidos ouvem.
17
Eu afirmo a vocês que isto é verdade: muitos profetas e muitas outras pessoas do povo de Deus gostariam de ver o que vocês estão vendo, mas não puderam; e gostariam de ouvir o que vocês estão ouvindo, mas não ouviram.
18
—Então escutem e aprendam o que a parábola do semeador quer dizer.
19
As pessoas que ouvem a mensagem do Reino, mas não a entendem, são como as sementes que foram semeadas na beira do caminho. O Maligno vem e tira o que foi semeado no coração delas.
20
As sementes que foram semeadas onde havia muitas pedras são as pessoas que ouvem a mensagem e a aceitam logo com alegria,
21
mas duram pouco porque não têm raiz. E, quando por causa da mensagem chegam os sofrimentos e as perseguições, elas logo abandonam a sua fé.
22
Outras pessoas são parecidas com as sementes que foram semeadas no meio dos espinhos. Elas ouvem a mensagem, mas as preocupações deste mundo e a ilusão das riquezas sufocam a mensagem, e essas pessoas não produzem frutos.
23
E as sementes que foram semeadas em terra boa são aquelas pessoas que ouvem, e entendem a mensagem, e produzem uma grande colheita: umas, cem; outras, sessenta; e ainda outras, trinta vezes mais do que foi semeado.
24
Jesus contou outra parábola. Ele disse ao povo: —O Reino do Céu é como um homem que semeou sementes boas nas suas terras.
25
Certa noite, quando todos estavam dormindo, veio um inimigo, semeou no meio do trigo uma erva ruim, chamada joio, e depois foi embora.
26
Quando as plantas cresceram, e se formaram as espigas, o joio apareceu.
27
Aí os empregados do dono das terras chegaram e disseram: “Patrão, o senhor semeou sementes boas nas suas terras. De onde será que veio este joio? ”
28
—“Foi algum inimigo que fez isso! ”, respondeu ele. —E eles perguntaram: “O senhor quer que a gente arranque o joio? ”
29
—“Não”, respondeu ele, “porque, quando vocês forem tirar o joio, poderão arrancar também o trigo.
30
Deixem o trigo e o joio crescerem juntos até o tempo da colheita. Então eu direi aos trabalhadores que vão fazer a colheita: ‘Arranquem primeiro o joio e amarrem em feixes para ser queimado. Depois colham o trigo e ponham no meu depósito. ’”
31
Jesus contou outra parábola. Ele disse ao povo: —O Reino do Céu é como uma semente de mostarda, que um homem pega e semeia na sua terra.
32
Ela é a menor de todas as sementes; mas, quando cresce, torna-se a maior de todas as plantas. Ela até chega a ser uma árvore, de modo que os passarinhos vêm e fazem ninhos nos seus ramos.
33
Jesus contou mais esta parábola para o povo: —O Reino do Céu é como o fermento que uma mulher pega e mistura em três medidas de farinha, até que ele se espalhe por toda a massa.
34
Jesus usava parábolas para dizer tudo isso ao povo. Ele não dizia nada a eles sem ser por meio de parábolas.
35
Isso aconteceu para se cumprir o que o profeta tinha dito: “Usarei parábolas quando falar com esse povo e explicarei coisas desconhecidas desde a criação do mundo. ”
36
Então Jesus deixou a multidão e voltou para casa. Os discípulos chegaram perto dele e perguntaram: —Conte para nós o que quer dizer a parábola do joio.
37
Jesus respondeu: —Quem semeia as sementes boas é o Filho do Homem.
38
O terreno é o mundo. As sementes boas são as pessoas que pertencem ao Reino; e o joio, as que pertencem ao Maligno.
39
O inimigo que semeia o joio é o próprio Diabo. A colheita é o fim dos tempos, e os que fazem a colheita são os anjos.
40
Assim como o joio é ajuntado e jogado no fogo, assim também será no fim dos tempos.
41
O Filho do Homem mandará os seus anjos, e eles ajuntarão e tirarão do seu Reino todos os que fazem com que os outros pequem e também todos os que praticam o mal.
42
Depois os anjos jogarão essas pessoas na fornalha de fogo, onde vão chorar e ranger os dentes de desespero.
43
Então o povo de Deus brilhará como o sol no Reino do seu Pai. Se vocês têm ouvidos para ouvir, então ouçam.
44
—O Reino do Céu é como um tesouro escondido num campo, que certo homem acha e esconde de novo. Fica tão feliz, que vende tudo o que tem, e depois volta, e compra o campo.
45
—O Reino do Céu é também como um comerciante que anda procurando pérolas finas.
46
Quando encontra uma pérola que é mesmo de grande valor, ele vai, vende tudo o que tem e compra a pérola.
47
—O Reino do Céu é ainda como uma rede que é jogada no lago. Ela apanha peixes de todos os tipos.
48
E, quando está cheia, os pescadores a arrastam para a praia e sentam para separar os peixes: os que prestam são postos dentro dos cestos, e os que não prestam são jogados fora.
49
No fim dos tempos também será assim: os anjos sairão, e separarão as pessoas más das boas,
50
e jogarão as pessoas más na fornalha de fogo. E ali elas vão chorar e ranger os dentes de desespero.
51
Então Jesus perguntou aos discípulos: —Vocês entenderam essas coisas? —Sim! —responderam eles.
52
Jesus disse: —Pois isso quer dizer que todo mestre da Lei que se torna discípulo no Reino do Céu é como um pai de família que tira do seu depósito coisas novas e coisas velhas.
53
Quando Jesus acabou de contar essas parábolas, saiu dali
54
e voltou para a cidade de Nazaré, onde ele tinha morado. Ele ensinava na sinagoga, e os que o ouviam ficavam admirados e perguntavam: —De onde vêm a sabedoria dele e o poder que ele tem para fazer milagres?
55
Por acaso ele não é o filho do carpinteiro? A sua mãe não é Maria? Ele não é irmão de Tiago, José, Simão e Judas?
56
Todas as suas irmãs não moram aqui? De onde é que ele consegue tudo isso?
57
Por isso ficaram desiludidos com ele. Mas Jesus disse: —Um profeta é respeitado em toda parte, menos na sua terra e na sua casa.
58
Jesus não pôde fazer muitos milagres ali porque eles não tinham fé.
Mateus 13 - Nova Tradução na Linguagem de Hoje - NTLH
