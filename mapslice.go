package main

import "gopkg.in/yaml.v2"
import "io/ioutil"
import "os"
import "fmt"

func main() {
	ms := func(filename string) (mapslice yaml.MapSlice) {
		in, _ := ioutil.ReadFile(filename)
		yaml.Unmarshal(in, &mapslice)
		return mapslice
	}
	niv := ms(os.Args[1])
	ntlh := ms(os.Args[2])
	for i := 0; i < len(niv); i++ {
		fmt.Println(i+1, niv[i].Key, ntlh[i].Key)
		for j := 0; j < len(niv[i].Value.(yaml.MapSlice)); j++ {
			fmt.Printf("  %d\n", j+1)
			//fmt.Println(" ", j+1, niv[i].Value.(yaml.MapSlice)[j].Key, ntlh[i].Value.(yaml.MapSlice)[j].Key)
			for k := 0; k < len(ntlh[i].Value.(yaml.MapSlice)[j].Value.(yaml.MapSlice)); k++ {
				fmt.Printf("    %d %s (%s)\n", k+1, ntlh[i].Value.(yaml.MapSlice)[j].Value.(yaml.MapSlice)[k].Value, niv[i].Value.(yaml.MapSlice)[j].Value.(yaml.MapSlice)[k].Value)
			}
		}
	}
}
