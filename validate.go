package main

import "fmt"
import "log"
import "gopkg.in/yaml.v2"
import "os"
import "io/ioutil"

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		usage()
		os.Exit(1)
	}
	in1, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalln(err)
	}
	in2, err := ioutil.ReadFile(args[1])
	if err != nil {
		log.Fatalln(err)
	}

	var yml1 map[string]map[int]map[int]string
	var yml2 map[string]map[int]map[int]string
	if err := yaml.Unmarshal(in1, &yml1); err != nil {
		log.Fatalln(err)
	}
	if err := yaml.Unmarshal(in2, &yml2); err != nil {
		log.Fatalln(err)
	}

	// check whether number of books match
	if len(yml1) != len(yml2) {
		log.Fatalln("stage1")
	}

	// check whether number of chapters match
	for i := 1; i <= len(yml1); i++ {
		nivKey := niv[i]
		ntlhKey := ntlh[i]
		if len(yml1[nivKey]) != len(yml2[ntlhKey]) {
			log.Fatalln("stage2 failed")
		}
	}

	// check whether number of sentences match
	for i := 1; i <= len(yml1); i++ {
		nivKey := niv[i]
		ntlhKey := ntlh[i]
		nivLen := len(yml1[nivKey])
		ntlhLen := len(yml2[ntlhKey])
		if nivLen != ntlhLen {
			log.Fatalln("stage2 failed")
		}
		for j := 1; j <= nivLen; j++ {
			if len(yml1[nivKey][j]) != len(yml2[ntlhKey][j]) {
				log.Println("stage3 failed", nivKey, ntlhKey, len(yml1[nivKey][j]), len(yml2[ntlhKey][j]), j)
			}
		}
	}
	/*
		for k, _ := range yml {
			fmt.Printf("\"%s\",\n", k)
		}
	*/

	/*
		for i, v := range niv {
			if i == 0 {
				continue
			}
			chapter := v
			// println(chapter)

			m := map[string]interface{}{}
			m[chapter] = yml[chapter]
			out, err := yaml.Marshal(m)
			if err != nil {
				log.Fatalln(err)
			}
			fmt.Print(string(out))
		}
	*/

	/*
		out, err := yaml.Marshal(yml)
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(string(out))
	*/
	fmt.Println("ok")
}

func usage() {
	fmt.Printf("usage: %s <yaml> <yaml>", os.Args[0])
}

var niv = [...]string{
	13: "1 Chronicles",
	46: "1 Corinthians",
	62: "1 John",
	11: "1 Kings",
	60: "1 Peter",
	9:  "1 Samuel",
	52: "1 Thessalonians",
	54: "1 Timothy",
	14: "2 Chronicles",
	47: "2 Corinthians",
	63: "2 John",
	12: "2 Kings",
	61: "2 Peter",
	10: "2 Samuel",
	53: "2 Thessalonians",
	55: "2 Timothy",
	64: "3 John",
	44: "Acts",
	30: "Amos",
	51: "Colossians",
	27: "Daniel",
	5:  "Deuteronomy",
	21: "Ecclesiastes",
	49: "Ephesians",
	17: "Esther",
	2:  "Exodus",
	26: "Ezekiel",
	15: "Ezra",
	48: "Galatians",
	1:  "Genesis",
	35: "Habakkuk",
	37: "Haggai",
	58: "Hebrews",
	28: "Hosea",
	23: "Isaiah",
	59: "James",
	24: "Jeremiah",
	18: "Job",
	29: "Joel",
	43: "John",
	32: "Jonah",
	6:  "Joshua",
	65: "Jude",
	7:  "Judges",
	25: "Lamentations",
	3:  "Leviticus",
	42: "Luke",
	39: "Malachi",
	41: "Mark",
	40: "Matthew",
	33: "Micah",
	34: "Nahum",
	16: "Nehemiah",
	4:  "Numbers",
	31: "Obadiah",
	57: "Philemon",
	50: "Philippians",
	20: "Proverbs",
	19: "Psalms",
	66: "Revelation",
	45: "Romans",
	8:  "Ruth",
	22: "Song of Solomon",
	56: "Titus",
	38: "Zechariah",
	36: "Zephaniah",
}

func init() {
	_ = niv
	_ = ntlh
	_ = ntlhLatin
}

var ntlh = [...]string{
	1:  "Gênesis",
	2:  "Êxodo",
	3:  "Levítico",
	4:  "Números",
	5:  "Deuteronômio",
	6:  "Josué",
	7:  "Juízes",
	8:  "Rute",
	9:  "1 Samuel",
	10: "2 Samuel",
	11: "1 Reis",
	12: "2 Reis",
	13: "1 Crônicas",
	14: "2 Crônicas",
	15: "Esdras",
	16: "Neemias",
	17: "Ester",
	18: "Jó",
	19: "Salmos",
	20: "Provérbios",
	21: "Eclesiastes",
	22: "Cânticos",
	23: "Isaías",
	24: "Jeremias",
	25: "Lamentações",
	26: "Ezequiel",
	27: "Daniel",
	28: "Oséias",
	29: "Joel",
	30: "Amós",
	31: "Obadias",
	32: "Jonas",
	33: "Miquéias",
	34: "Naum",
	35: "Habacuque",
	36: "Sofonias",
	37: "Ageu",
	38: "Zacarias",
	39: "Malaquias",
	40: "Mateus",
	41: "Marcos",
	42: "Lucas",
	43: "João",
	44: "Atos",
	45: "Romanos",
	46: "1 Coríntios",
	47: "2 Coríntios",
	48: "Gálatas",
	49: "Efésios",
	50: "Filipenses",
	51: "Colossenses",
	52: "1 Tessalonicenses",
	53: "2 Tessalonicenses",
	54: "1 Timóteo",
	55: "2 Timóteo",
	56: "Tito",
	57: "Filemom",
	58: "Hebreus",
	59: "Tiago",
	60: "1 Pedro",
	61: "2 Pedro",
	62: "1 João",
	63: "2 João",
	64: "3 João",
	65: "Judas",
	66: "Apocalipse",
}

var ntlhLatin = [...]string{
	1:  "Genesis",
	2:  "Exodo",
	3:  "Levitico",
	4:  "Numeros",
	5:  "Deuteronomio",
	6:  "Josue",
	7:  "Juizes",
	8:  "Rute",
	9:  "1-Samuel",
	10: "2-Samuel",
	11: "1-Reis",
	12: "2-Reis",
	13: "1-Cronicas",
	14: "2-Cronicas",
	15: "Esdras",
	16: "Neemias",
	17: "Ester",
	18: "Jo",
	19: "Salmos",
	20: "Proverbios",
	21: "Eclesiastes",
	22: "Canticos",
	23: "Isaias",
	24: "Jeremias",
	25: "Lamentacoes",
	26: "Ezequiel",
	27: "Daniel",
	28: "Oseias",
	29: "Joel",
	30: "Amos",
	31: "Obadias",
	32: "Jonas",
	33: "Miqueias",
	34: "Naum",
	35: "Habacuque",
	36: "Sofonias",
	37: "Ageu",
	38: "Zacarias",
	39: "Malaquias",
	40: "Mateus",
	41: "Marcos",
	42: "Lucas",
	43: "Joao",
	44: "Atos",
	45: "Romanos",
	46: "1-Corintios",
	47: "2-Corintios",
	48: "Galatas",
	49: "Efesios",
	50: "Filipenses",
	51: "Colossenses",
	52: "1-Tessalonicenses",
	53: "2-Tessalonicenses",
	54: "1-Timoteo",
	55: "2-Timoteo",
	56: "Tito",
	57: "Filemom",
	58: "Hebreus",
	59: "Tiago",
	60: "1-Pedro",
	61: "2-Pedro",
	62: "1-Joao",
	63: "2-Joao",
	64: "3-Joao",
	65: "Judas",
	66: "Apocalipse",
}
