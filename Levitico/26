Levítico 26
1
O SENHOR Deus disse ao povo de Israel: —Não façam nenhum ídolo ou imagem, nem coluna sagrada ou pedra com figuras gravadas para adorar. Não adorem nenhum deles; eu, o SENHOR, sou o Deus de vocês.
2
Guardem o sábado, o meu dia, e respeitem o lugar onde sou adorado. Eu sou o SENHOR.
3
—Se vocês obedecerem às minhas leis e aos meus mandamentos, fazendo tudo o que eu ordeno,
4
eu mandarei chuva no tempo certo, a terra produzirá colheitas, e as árvores darão frutas.
5
As colheitas serão tão grandes, que vocês ainda estarão colhendo cereais quando chegar o tempo de colher uvas e estarão colhendo uvas quando chegar o tempo de semear os campos. Haverá bastante comida para todos, e vocês viverão em segurança na sua terra.
6
—Eu darei paz à terra de vocês. Todos dormirão sossegados, e ninguém os assustará, pois farei desaparecer da terra os animais selvagens e acabarei com as guerras.
7
Vocês vencerão os seus inimigos e os matarão;
8
cinco de vocês derrotarão cem deles, e cem de vocês derrotarão dez mil. Todos eles serão mortos.
9
Eu abençoarei vocês e lhes darei muitos filhos. Cumprirei as promessas da aliança que fiz com vocês.
10
As colheitas serão tão grandes, que vocês precisarão jogar fora o trigo velho para terem lugar onde guardar o novo.
11
Morarei no meio de vocês, na minha Tenda Sagrada, e nunca os abandonarei.
12
Estarei sempre com vocês; vocês serão o meu povo, e eu serei o seu Deus.
13
Eu sou o SENHOR, o Deus de vocês. Eu os tirei do Egito para que vocês não fossem escravos dos egípcios. Eu os livrei da escravidão e os fiz andar de cabeça erguida.
14
—Porém, se vocês não obedecerem a todos os meus mandamentos,
15
se rejeitarem as minhas leis, se desprezarem as minhas ordens e se quebrarem a aliança que fiz com vocês,
16
então eu os castigarei. Mandarei desastres, e doenças, e febres que abalam a saúde e enfraquecem o corpo. Não adiantará nada semear os campos, pois os inimigos é que comerão as colheitas.
17
Ficarei contra vocês e deixarei que sejam derrotados pelos inimigos. Eles os dominarão, e vocês fugirão mesmo quando ninguém os perseguir.
18
—Porém, se nem assim vocês me obedecerem, mas continuarem a pecar, eu mandarei um castigo sete vezes pior.
19
Acabarei com o seu poder, de que vocês se orgulham; não mandarei chuva, e o chão ficará duro como ferro.
20
Não adiantará nada vocês trabalharem e se cansarem; os campos não produzirão colheitas, e as árvores não darão frutas.
21
—E, se ainda assim vocês teimarem em pecar, em me rejeitar e em desobedecer aos meus mandamentos, eu mandarei um castigo sete vezes pior.
22
Mandarei para o meio de vocês animais selvagens que matarão os seus filhos, acabarão com o seu gado e matarão tanta gente, que não haverá ninguém para andar pelas estradas.
23
—E, se mesmo com isso vocês não voltarem para mim, mas continuarem a me desafiar,
24
eu ficarei contra vocês e por causa dos seus pecados mandarei um castigo sete vezes pior.
25
Ordenarei que povos inimigos os ataquem; e assim vocês serão castigados por terem quebrado a aliança que fiz com vocês. E, se vocês se ajuntarem nas cidades para escaparem dos inimigos, eu farei com que vocês sejam atacados por doenças graves, e os inimigos os prenderão.
26
Por causa do meu castigo, a comida será tão pouca, que bastará um forno para dez donas de casa assarem pão, e cada pessoa receberá uma porção tão pequena de comida, que ninguém conseguirá matar a fome.
27
—E, se nem assim vocês me obedecerem, mas continuarem a me desafiar,
28
cheio de ira eu ficarei contra vocês e por causa dos seus pecados mandarei um castigo sete vezes pior.
29
Haverá tanta falta de comida, que vocês devorarão os seus próprios filhos.
30
Eu ficarei tão irado com vocês, que destruirei os lugares onde vocês adoram deuses pagãos, quebrarei os altares em que é queimado incenso e jogarei os corpos de vocês em cima dos ídolos caídos.
31
Destruirei as cidades e as deixarei em ruínas; derrubarei os seus templos e não aceitarei os sacrifícios que vocês me oferecerem.
32
Arrasarei tão completamente a terra em que vocês moram, que os inimigos que vierem morar nela ficarão espantados.
33
Eu farei com que haja guerra, e vocês serão espalhados pelas outras nações; na terra de vocês não haverá moradores, e as cidades ficarão em ruínas.
34
Assim, a terra terá os seus anos de descanso. Enquanto estiver sem moradores e vocês estiverem espalhados pelas nações estrangeiras, a terra terá os seus anos de descanso;
35
pois ela não descansou durante o tempo em que vocês moraram nela.
36
—Quanto aos que continuarem vivos em terras estrangeiras, eu farei com que eles fiquem com tanto medo, que até mesmo o som de folhas caindo no chão os fará fugir; fugirão como se viessem inimigos atrás deles e cairão sem que ninguém os persiga.
37
Uns cairão em cima dos outros, como se estivessem combatendo, mesmo que não haja inimigos por perto. E, quando os inimigos chegarem, vocês não terão forças para lutar contra eles.
38
Vocês morrerão em terras estrangeiras e ali serão sepultados.
39
Os que ficarem vivos naquelas terras serão finalmente destruídos pelos seus próprios pecados e pelos pecados dos seus antepassados.
40
—Mas os descendentes de vocês confessarão os seus pecados e também os pecados dos seus antepassados, isto é, daqueles que foram infiéis a mim e desobedeceram às minhas ordens,
41
fazendo com que eu ficasse contra eles e os espalhasse pelos países dos seus inimigos. Portanto, se eles se arrependerem das suas idéias e dos seus costumes pagãos e se aceitarem o castigo pelos seus pecados,
42
então eu lembrarei das alianças que fiz com Jacó e com Isaque e com Abraão e lembrarei também da Terra Prometida.
43
—Mas, enquanto os descendentes de vocês estiverem espalhados por terras estrangeiras, a Terra Prometida ficará deserta e terá os seus anos de descanso. Os seus descendentes desobedeceram às minhas leis e desprezaram os meus mandamentos; por isso pagarão pelos seus pecados.
44
Mas, mesmo quando estiverem em terras estrangeiras, eu não os abandonarei, nem os destruirei, pois não quebrarei a aliança que fiz com eles. Eu sou o SENHOR, o Deus deles.
45
Pelo contrário, eu lembrarei da aliança que fiz com os seus antepassados, quando os tirei do Egito a fim de ser o seu Deus, mostrando assim o meu poder às outras nações. Eu sou Deus, o SENHOR.
46
São esses os mandamentos, as leis e as ordens que o SENHOR Deus deu aos israelitas por meio de Moisés, no monte Sinai.
Levítico 26 - Nova Tradução na Linguagem de Hoje - NTLH
