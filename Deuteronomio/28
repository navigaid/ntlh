Deuteronômio 28
1
Moisés disse ao povo: —Se vocês derem atenção a tudo o que o SENHOR, nosso Deus, está dizendo a vocês e se obedecerem fielmente a todos os seus mandamentos que eu lhes estou dando hoje, Deus fará com que sejam mais poderosos do que qualquer outra nação do mundo.
2
Obedeçam ao SENHOR Deus, e ele lhes dará todas estas bênçãos:
3
—Deus os abençoará nas cidades e nos campos.
4
—Deus os abençoará dando-lhes muitos filhos, boas colheitas e muitas crias de gado e de ovelhas.
5
—Deus os abençoará com boas colheitas de trigo e de cevada e com muita comida.
6
—Deus os abençoará em tudo o que fizerem.
7
—Quando os inimigos atacarem, o SENHOR Deus os destruirá na presença de vocês. Eles atacarão juntos, em ordem, mas fugirão para todos os lados, em desordem.
8
—O SENHOR, nosso Deus, abençoará vocês em tudo o que fizerem e lhes dará tanto trigo, que os seus depósitos ficarão cheios. Ele os abençoará ricamente na terra que está dando a vocês.
9
—Se obedecerem a todas as leis do SENHOR, nosso Deus, e cumprirem todas as suas ordens, ele fará com que sejam o seu único povo, o povo escolhido, como prometeu com juramento a vocês.
10
Todos os outros povos do mundo verão que vocês pertencem a Deus, o SENHOR, e terão medo de vocês.
11
Ele lhes dará muitos filhos, muitos animais e boas colheitas na terra que está dando a vocês, de acordo com o juramento que fez aos nossos antepassados.
12
Deus abrirá o céu, onde guarda as suas ricas bênçãos, e lhes dará chuvas no tempo certo e assim abençoará o trabalho que vocês fizerem. Vocês emprestarão a muitas nações, porém não tomarão emprestado de ninguém.
13
Se obedecerem fielmente a todos os mandamentos do SENHOR Deus que hoje eu estou dando a vocês, ele fará com que fiquem no primeiro lugar entre as nações e não no último; e fará também com que a fama de vocês sempre cresça e nunca diminua.
14
Não se desviem desses mandamentos que hoje eu estou dando a vocês, nem para um lado nem para o outro, e nunca adorem nem sirvam outros deuses.
15
—Porém, se vocês não derem atenção ao que o SENHOR, nosso Deus, está mandando e não obedecerem às suas leis e aos seus mandamentos que lhes estou dando hoje, vocês serão castigados com as seguintes maldições:
16
—Deus os amaldiçoará nas cidades e nos campos.
17
—Deus os amaldiçoará dando-lhes pequenas colheitas de trigo e de cevada e pouco alimento.
18
—Deus os amaldiçoará dando-lhes poucos filhos, colheitas pequenas e poucas crias de gado e de ovelhas.
19
—Deus os amaldiçoará em tudo o que fizerem.
20
—Se vocês abandonarem o SENHOR e começarem a praticar maldades, ele fará cair sobre vocês maldição, confusão e castigo. Ele os amaldiçoará em tudo o que fizerem e acabará logo com vocês.
21
O SENHOR os castigará com doenças e mais doenças, até que todos morram na terra que vão invadir e que vai ser de vocês.
22
Ele os castigará com doenças contagiosas, infecções, inflamações e febres; mandará secas e ventos muito quentes; e fará com que pragas ataquem as plantas. Essas desgraças continuarão até que vocês morram.
23
Não haverá chuva, e o chão ficará duro como ferro.
24
Em vez de chuva, o SENHOR Deus mandará pó e areia sobre a terra, até que vocês sejam destruídos.
25
O SENHOR fará com que sejam derrotados pelos inimigos. Vocês atacarão juntos, em ordem, mas fugirão para todos os lados, em desordem. Todos os povos do mundo ficarão espantados quando souberem do que aconteceu com vocês.
26
Vocês morrerão, e o corpo de vocês servirá de comida para as aves e para os animais ferozes, e ninguém os enxotará.
27
O SENHOR Deus castigará vocês com tumores, como fez com os egípcios; vocês sofrerão de úlceras, chagas e coceiras sem cura.
28
O SENHOR os castigará, fazendo com que fiquem loucos, cegos e confusos.
29
Ao meio-dia vocês andarão sem rumo, como um cego apalpando o caminho. Vocês fracassarão em tudo o que fizerem, serão perseguidos e explorados a vida inteira, e não haverá ninguém que os socorra.
30
—Você contratará casamento, mas outro homem terá relações com a moça; você construirá uma casa, mas não morará nela; plantará uma parreira, mas não colherá as uvas.
31
Você verá o seu gado sendo morto, mas não comerá da carne. Verá outros levarem embora os seus jumentos, e estes não serão devolvidos. Os inimigos levarão as suas ovelhas, e não haverá ninguém para socorrê-lo.
32
—Na presença de vocês, estrangeiros pegarão os seus filhos e as suas filhas e os levarão embora como escravos. Vocês morrerão de saudade deles, mas não poderão fazer nada para trazê-los de volta.
33
Estrangeiros virão e levarão os cereais que com tanto trabalho vocês plantaram e colheram. Todos os dias vocês serão maltratados e perseguidos
34
e ficarão loucos por causa dos maus tratos que vão receber.
35
O SENHOR Deus os castigará com tumores incuráveis nas pernas; da ponta dos pés até a cabeça, o corpo de vocês ficará coberto de feridas que nunca saram.
36
—O SENHOR Deus levará todos vocês, junto com o rei que tiverem escolhido, para um país estrangeiro, que nem vocês nem os seus antepassados conheciam. Ali vocês adorarão deuses de madeira e de pedra.
37
Os povos dos países para onde o SENHOR os levar ficarão espantados quando souberem do que aconteceu com vocês. Eles falarão mal e zombarão de vocês.
38
—Vocês plantarão muitas sementes, mas a colheita será pequena porque os gafanhotos acabarão com tudo.
39
Vocês farão plantações de uvas e cuidarão delas, mas não colherão as uvas, nem beberão do vinho, pois os bichos acabarão com as plantas.
40
No país inteiro haverá muitas oliveiras, mas vocês não terão azeite para se ungirem, pois as azeitonas cairão das árvores antes de ficarem maduras.
41
Vocês terão filhos e filhas, mas estrangeiros os levarão como prisioneiros.
42
Os gafanhotos destruirão todas as árvores e todas as plantas da terra de vocês.
43
—Os estrangeiros que moram no meio de vocês ficarão cada vez mais poderosos, ao passo que vocês ficarão cada vez mais fracos.
44
Eles emprestarão a vocês, mas não tomarão emprestado de vocês; eles ficarão no primeiro lugar entre as nações, e vocês ficarão no último.
45
Moisés disse ao povo: —Todas essas maldições cairão sobre vocês e os perseguirão até os destruírem completamente. Pois vocês não deram atenção ao que o SENHOR, nosso Deus, disse e não obedeceram às leis e aos mandamentos que deu a vocês.
46
Essas desgraças serão para sempre a prova de que Deus castigou vocês e os seus descendentes.
47
O SENHOR lhes dará tudo o que é bom; mas, se vocês não o servirem com alegria e gratidão,
48
serão escravos dos inimigos que o SENHOR mandará contra vocês. Vocês os servirão com fome e com sede, sem roupa e precisando de tudo. Deus tratará vocês com crueldade até os destruir.
49
—O SENHOR Deus fará vir contra vocês, de longe, lá do fim do mundo, um povo estrangeiro que fala uma língua que vocês não entendem. Eles os atacarão como uma águia.
50
São gente cruel, que não respeita os velhos, nem tem pena dos moços.
51
Eles vão devorar todo o seu gado e todas as suas colheitas, e vocês morrerão de fome. Eles não deixarão para vocês nem cereais, nem vinho, nem azeite, nem crias de vacas e de ovelhas.
52
Cercarão as cidades onde vocês moram, até que caiam as muralhas altas e fortes em que vocês confiavam. Todas as cidades da terra que o SENHOR, nosso Deus, lhes deu serão cercadas pelo inimigo.
53
O cerco será terrível. Os moradores das cidades ficarão tão desesperados, que comerão os próprios filhos que Deus lhes deu.
54
( 54 - 55 ) A situação será tão terrível, que não haverá nada para comer. Até o homem mais educado e carinhoso ficará tão desesperado, que comerá os próprios filhos, e será tão mau, que não repartirá a carne com os irmãos, nem com a sua querida mulher, nem com os outros filhos.
55
( 54 - 55 ) A situação será tão terrível, que não haverá nada para comer. Até o homem mais educado e carinhoso ficará tão desesperado, que comerá os próprios filhos, e será tão mau, que não repartirá a carne com os irmãos, nem com a sua querida mulher, nem com os outros filhos.
56
( 56 - 57 ) A situação será tão terrível, que até a mulher mais delicada e carinhosa, que nunca precisou andar a pé quando saía de casa, ficará tão desesperada, que comerá, às escondidas, seu bebê recém-nascido e a placenta também. Ela será tão má, que não dará nenhum pedaço para o seu querido marido, nem para os outros filhos.
57
( 56 - 57 ) A situação será tão terrível, que até a mulher mais delicada e carinhosa, que nunca precisou andar a pé quando saía de casa, ficará tão desesperada, que comerá, às escondidas, seu bebê recém-nascido e a placenta também. Ela será tão má, que não dará nenhum pedaço para o seu querido marido, nem para os outros filhos.
58
—Se vocês não cumprirem todas as leis que estão escritas neste livro e se não respeitarem o nome do SENHOR, nosso Deus, esse nome que é glorioso e causa medo,
59
então ele castigará vocês e os seus descendentes com pragas e com doenças terríveis e demoradas.
60
Ele os castigará com as mesmas doenças com que castigou os egípcios, doenças que não têm cura e que deixam vocês com tanto medo.
61
O SENHOR mandará também doenças e pragas que não estão escritas neste Livro da Lei e os destruirá.
62
Vocês, que já foram tão numerosos como as estrelas do céu, ficarão um punhado de gente porque não obedeceram às ordens do SENHOR, nosso Deus.
63
E assim como o SENHOR tinha prazer em abençoá-los e fazer com que aumentassem em número, assim também terá prazer em castigá-los e destruí-los. Vocês serão arrancados da terra que vão invadir e que vai ser de vocês.
64
—O SENHOR Deus os espalhará por todos os países, de uma ponta do mundo à outra. Ali vocês adorarão deuses de madeira e de pedra, que nem vocês nem os seus antepassados conheciam.
65
E também nesses países vocês não terão sossego nem paz; o SENHOR fará com que fiquem cheios de aflição, desespero e medo.
66
A vida de vocês estará sempre em perigo; dia e noite o medo os acompanhará, e vocês ficarão desesperados da vida.
67
Vocês terão tanto medo e verão coisas tão terríveis, que de manhã dirão: “Tomara que já fosse noite! ” E ao cair a noite dirão: “Quem dera que já fosse dia! ”
68
O SENHOR Deus os fará voltar em navios para o Egito, ainda que ele tenha dito que vocês nunca mais iriam para lá. Ali vocês procurarão vender-se como escravos e escravas aos seus inimigos, os egípcios, mas ninguém vai querer comprá-los.
Deuteronômio 28 - Nova Tradução na Linguagem de Hoje - NTLH
