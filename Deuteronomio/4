Deuteronômio 4
1
Depois Moisés disse ao povo de Israel: —Obedeçam a todas as leis e a todas as ordens que eu estou dando a vocês agora, para que vivam e tomem posse da terra que o SENHOR, o Deus dos nossos antepassados, vai dar a vocês.
2
Não acrescentem nada à lei que lhes estou dando, nem tirem dela uma só palavra. Guardem todos os mandamentos do SENHOR, nosso Deus.
3
Vocês mesmos viram o que o SENHOR fez perto do monte Peor, como matou todas as pessoas do nosso povo que ali adoraram o deus Baal.
4
Mas aqueles que continuaram fiéis a Deus, o SENHOR, ainda estão vivos.
5
—Como o SENHOR, meu Deus, me ordenou, eu lhes tenho ensinado as leis e os mandamentos que vocês deverão guardar na terra que vão invadir e que vai ser de vocês.
6
Portanto, obedeçam fielmente a todas essas leis, e assim os outros povos verão que vocês são sábios e inteligentes. Quando ouvirem falar dessas leis, eles dirão: “Como é sábio e inteligente o povo dessa grande nação! ”
7
—Nenhuma outra grande nação tem um deus que fique tão perto do seu povo como o SENHOR, nosso Deus, fica perto de nós. Ele nos ouve todas as vezes que pedimos a sua ajuda.
8
E será que existe outra grande nação que tenha mandamentos e ensinamentos tão direitos como essa lei que estou lhes dando hoje?
9
Portanto, tenham cuidado e sejam fiéis para que nunca esqueçam as coisas que viram. E contem aos seus filhos e netos
10
o que aconteceu no monte Sinai naquele dia em que vocês estiveram na presença do SENHOR, nosso Deus, quando ele me disse: “Reúna esse povo na minha presença para que escutem o que vou dizer, a fim de que aprendam a temer-me a vida inteira e assim ensinem os seus filhos. ”
11
—Então vocês foram e ficaram ao pé do monte Sinai, que estava completamente coberto de escuridão e de nuvens negras. Em cima do monte havia um fogo, e as suas chamas subiam até o céu.
12
Do meio do fogo o SENHOR Deus falou com vocês; vocês ouviram a voz dele, mas não viram ninguém; só escutaram a voz.
13
Deus lhes anunciou a aliança que estava fazendo com vocês e mandou que obedecessem aos dez mandamentos, que depois escreveu em duas placas de pedra.
14
E ao mesmo tempo o SENHOR mandou que eu lhes ensinasse as leis e os mandamentos que vocês devem seguir na terra que vão invadir e que vai ser de vocês.
15
Moisés continuou: —Quando o SENHOR, nosso Deus, falou com vocês do meio do fogo no monte Sinai, vocês não viram a forma de ninguém. Portanto, tenham todo o cuidado
16
e não cometam o erro de fazer imagens para adorar. Não façam nenhuma imagem que sirva de ídolo, seja em forma de homem, ou de mulher,
17
ou de animal, ou de ave,
18
ou de animal que se arrasta pelo chão, ou de peixe.
19
E, quando olharem para o céu, não caiam na tentação de adorar o sol, a lua ou as estrelas. Pois o SENHOR, nosso Deus, repartiu o sol, a lua e as estrelas entre os outros povos, para que eles os adorem.
20
Mas vocês são o povo que o SENHOR tirou do Egito, aquela fornalha acesa, para serem somente dele, como, de fato, são.
21
Por causa de vocês o SENHOR Deus ficou irado comigo e jurou que eu nunca atravessaria o rio Jordão, nem entraria na boa terra que o SENHOR, nosso Deus, lhes está dando.
22
Eu não vou atravessar o rio Jordão; vou morrer aqui mesmo. Mas vocês vão atravessá-lo e tomar posse daquela boa terra.
23
Tenham o cuidado de não esquecerem a aliança que o SENHOR, nosso Deus, fez com vocês. Obedeçam à sua ordem e não façam nenhuma imagem para adorar.
24
Pois o SENHOR, nosso Deus, é um fogo destruidor; ele não tolera outros deuses.
25
—E mesmo depois de muitos anos na terra de Canaã, quando vocês já estiverem velhos e tiverem filhos e netos, não cometam o erro de fazer ídolos. Para Deus isso é um pecado grave, e ele ficará irado com vocês.
26
Chamo o céu e a terra como testemunhas contra vocês: se adorarem ídolos, vocês desaparecerão logo da terra que vai ser de vocês no outro lado do rio Jordão. Vocês viverão pouco tempo naquela terra e logo serão completamente destruídos.
27
O SENHOR Deus os espalhará pelas nações estrangeiras, onde poucos de vocês ficarão vivos.
28
Naquelas nações vocês adorarão deuses feitos de madeira e de pedra, que não vêem, nem ouvem, não comem, nem cheiram.
29
Lá vocês procurarão o SENHOR, seu Deus, e o encontrarão, se o buscarem com todo o coração e com toda a alma.
30
E, no futuro, quando estiverem em dificuldades, e tudo isso acontecer, então se vocês voltarem para o SENHOR, nosso Deus, e obedecerem aos seus mandamentos,
31
ele não os abandonará. Ele é Deus misericordioso e não os destruirá, nem esquecerá a aliança que fez com os nossos antepassados e que jurou cumprir.
32
—Estudem o passado, toda a história desde a criação da humanidade. Caminhem pelo mundo inteiro e perguntem se alguém já viu ou ouviu falar de haver acontecido alguma coisa tão impressionante como esta.
33
Será que já houve alguém que, depois de ter ouvido um deus falando do meio do fogo, ainda continuasse vivo, como aconteceu com vocês?
34
Será que já houve um deus que resolveu ir tirar do meio de outra nação um povo para ser completamente dele, como o SENHOR, nosso Deus, fez com vocês? Vocês viram como ele mostrou o seu poder e a sua força; viram como ele, por meio de pragas e milagres maravilhosos, de guerras e feitos espantosos, tirou vocês do Egito.
35
Deus deixou que vocês vissem tudo isso para que soubessem que o SENHOR é Deus; não há nenhum outro deus, a não ser ele.
36
Para ensiná-los, Deus falou do céu, e na terra ele lhes mostrou um grande fogo, e do meio desse fogo falou com vocês.
37
Deus amou os nossos antepassados e por isso escolheu vocês; e ele mesmo, com a sua grande força, os tirou do Egito.
38
Depois foi na frente de vocês, expulsando povos que eram mais numerosos e mais poderosos do que vocês, e assim deu a vocês as terras daquelas nações, onde vocês estão morando agora.
39
—Fiquem sabendo agora e nunca esqueçam isto: somente o SENHOR é Deus lá em cima no céu e aqui embaixo na terra. Não há outro deus.
40
Portanto, obedeçam a todas as suas leis que eu lhes estou dando hoje. Assim vocês e os seus descendentes serão felizes e viverão muitos anos na terra que o SENHOR, nosso Deus, lhes está dando para sempre.
41
Depois Moisés escolheu três cidades no lado leste do rio Jordão
42
para onde poderia fugir qualquer homem que, sem querer ou por engano, tivesse matado alguém de quem não tinha ódio. Em qualquer uma dessas cidades esse homem estaria seguro, e ninguém poderia matá-lo.
43
Para a tribo de Rúben, Moisés escolheu Bezer, no deserto, no planalto; para a tribo de Gade, ele escolheu Ramote, na região de Gileade; e, para a tribo de Manassés do Leste, ele escolheu Golã, na região de Basã.
44
Moisés deu ao povo de Israel a lei de Deus,
45
com os seus mandamentos, ordens e ensinamentos. Isso foi depois que os israelitas tinham saído do Egito
46
e haviam chegado ao vale que fica perto de Bete-Peor, na região a leste do rio Jordão. Essa terra era de Seom, o rei dos amorreus, que morava em Hesbom. Moisés e os israelitas derrotaram Seom
47
e tomaram posse da sua terra. E fizeram a mesma coisa com Ogue, rei de Basã. Assim os israelitas invadiram e ocuparam as terras desses dois reis amorreus, a leste do rio Jordão.
48
As terras deles iam desde a cidade de Aroer, que fica perto do vale do rio Arnom, no Sul, até o monte Siriom ( isto é, o monte Hermom ), no Norte.
49
Fazia parte delas a região a leste do rio Jordão até o mar Morto, no Sul, e até o pé do monte Pisga, no Leste.
Deuteronômio 4 - Nova Tradução na Linguagem de Hoje - NTLH
