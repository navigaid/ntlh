João 11
1
Um homem chamado Lázaro estava doente. Ele era do povoado de Betânia, onde Maria e a sua irmã Marta moravam.
2
( Esta Maria era a mesma que pôs perfume nos pés do Senhor Jesus e os enxugou com os seus cabelos. Era o irmão dela, Lázaro, que estava doente. )
3
As duas irmãs mandaram dizer a Jesus: —Senhor, o seu querido amigo Lázaro está doente!
4
Quando Jesus recebeu a notícia, disse: —O resultado final dessa doença não será a morte de Lázaro. Isso está acontecendo para que Deus revele o seu poder glorioso; e assim, por causa dessa doença, a natureza divina do Filho de Deus será revelada.
5
Jesus amava muito Marta, e a sua irmã, e também Lázaro.
6
Porém quando soube que Lázaro estava doente, ainda ficou dois dias onde estava.
7
Então disse aos seus discípulos: —Vamos voltar para a Judéia.
8
Mas eles disseram: —Mestre, faz tão pouco tempo que o povo de lá queria matá-lo a pedradas, e o senhor quer voltar?
9
Jesus respondeu: —Por acaso o dia não tem doze horas? Se alguém anda de dia não tropeça porque vê a luz deste mundo.
10
Mas, se anda de noite, tropeça porque nele não existe luz.
11
Jesus disse isso e depois continuou: —O nosso amigo Lázaro está dormindo, mas eu vou lá acordá-lo.
12
—Senhor, se ele está dormindo, isso quer dizer que vai ficar bom! —disseram eles.
13
Mas o que Jesus queria dizer era que Lázaro estava morto. Porém eles pensavam que ele estivesse falando do sono natural.
14
Então Jesus disse claramente: —Lázaro morreu,
15
mas eu estou alegre por não ter estado lá com ele, pois assim vocês vão crer. Vamos até a casa dele.
16
Então Tomé, chamado “o Gêmeo”, disse aos outros discípulos: —Vamos nós também a fim de morrer com o Mestre!
17
Quando Jesus chegou, já fazia quatro dias que Lázaro havia sido sepultado.
18
Betânia ficava a menos de três quilômetros de Jerusalém,
19
e muitas pessoas tinham vindo visitar Marta e Maria para as consolarem por causa da morte do irmão.
20
Quando Marta soube que Jesus estava chegando, foi encontrar-se com ele. Porém Maria ficou sentada em casa.
21
Então Marta disse a Jesus: —Se o senhor estivesse aqui, o meu irmão não teria morrido!
22
Mas eu sei que, mesmo assim, Deus lhe dará tudo o que o senhor pedir a ele.
23
—O seu irmão vai ressuscitar! —disse Jesus.
24
Marta respondeu: —Eu sei que ele vai ressuscitar no último dia!
25
Então Jesus afirmou: —Eu sou a ressurreição e a vida. Quem crê em mim, ainda que morra, viverá;
26
e quem vive e crê em mim nunca morrerá. Você acredita nisso?
27
—Sim, senhor! —disse ela. —Eu creio que o senhor é o Messias, o Filho de Deus, que devia vir ao mundo.
28
Depois de dizer isso, Marta foi, chamou Maria, a sua irmã, e lhe disse em particular: —O Mestre chegou e está chamando você.
29
Quando Maria ouviu isso, levantou-se depressa e foi encontrar-se com Jesus.
30
Pois ele não tinha chegado ao povoado, mas ainda estava no lugar onde Marta o havia encontrado.
31
As pessoas que estavam na casa com Maria, consolando-a, viram que ela se levantou e saiu depressa. Então foram atrás dela, pois pensavam que ela ia ao túmulo para chorar ali.
32
Maria chegou ao lugar onde Jesus estava e logo que o viu caiu aos pés dele e disse: —Se o senhor tivesse estado aqui, o meu irmão não teria morrido!
33
Jesus viu Maria chorando e viu as pessoas que estavam com ela chorando também. Então ficou muito comovido e aflito
34
e perguntou: —Onde foi que vocês o sepultaram? —Venha ver, senhor! —responderam.
35
Jesus chorou.
36
Então as pessoas disseram: —Vejam como ele amava Lázaro!
37
Mas algumas delas disseram: —Ele curou o cego. Será que não poderia ter feito alguma coisa para que Lázaro não morresse?
38
Jesus ficou outra vez muito comovido. Ele foi até o túmulo, que era uma gruta com uma pedra colocada na entrada,
39
e ordenou: —Tirem a pedra! Marta, a irmã do morto, disse: —Senhor, ele está cheirando mal, pois já faz quatro dias que foi sepultado!
40
Jesus respondeu: —Eu não lhe disse que, se você crer, você verá a revelação do poder glorioso de Deus?
41
Então tiraram a pedra. Jesus olhou para o céu e disse: —Pai, eu te agradeço porque me ouviste.
42
Eu sei que sempre me ouves; mas eu estou dizendo isso por causa de toda esta gente que está aqui, para que eles creiam que tu me enviaste.
43
Depois de dizer isso, gritou: —Lázaro, venha para fora!
44
E o morto saiu. Os seus pés e as suas mãos estavam enfaixados com tiras de pano, e o seu rosto estava enrolado com um pano. Então Jesus disse: —Desenrolem as faixas e deixem que ele vá.
45
Muitas pessoas que tinham ido visitar Maria viram o que Jesus tinha feito e creram nele.
46
Mas algumas pessoas voltaram e contaram aos fariseus o que ele havia feito.
47
Então os fariseus e os chefes dos sacerdotes se reuniram com o Conselho Superior e disseram: —O que é que nós vamos fazer? Esse homem está fazendo muitos milagres!
48
Se deixarmos que ele continue fazendo essas coisas, todos vão crer nele. Aí as autoridades romanas agirão contra nós e destruirão o Templo e o nosso país.
49
Então Caifás, que naquele ano era o Grande Sacerdote, disse: —Vocês não sabem nada!
50
Será que não entendem que para vocês é melhor que morra apenas um homem pelo povo do que deixar que o país todo seja destruído?
51
Naquele momento Caifás não estava falando por si mesmo. Mas, como ele era o Grande Sacerdote naquele ano, estava profetizando que Jesus ia morrer pela nação.
52
E não somente pela nação, mas também para reunir em um só corpo todos os filhos de Deus que estão espalhados por toda parte.
53
Então, daquele dia em diante, os líderes judeus fizeram planos para matar Jesus.
54
Por isso ele já não andava publicamente na Judéia, mas foi para uma região perto do deserto, a uma cidade chamada Efraim, e ficou ali com os seus discípulos.
55
Faltava pouco tempo para a Festa da Páscoa. Muitos judeus foram a Jerusalém antes da festa para tomar parte na cerimônia de purificação.
56
Eles procuravam Jesus e, no pátio do Templo, perguntavam uns aos outros: —O que é que vocês acham? Será que ele vem à festa?
57
Os chefes dos sacerdotes e os fariseus queriam prender Jesus. Por isso tinham dado ordem para que, se alguém soubesse, contasse onde ele estava.
João 11 - Nova Tradução na Linguagem de Hoje - NTLH
