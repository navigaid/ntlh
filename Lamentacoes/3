Lamentações 3
1
Eu sou aquele que sabe o que é sofrer os golpes da ira de Deus.
2
Ele me levou para a escuridão e me fez andar por caminhos sem luz.
3
Com a sua mão, me bateu muitas vezes, o dia inteiro.
4
Deus fez envelhecer a minha carne e a minha pele e quebrou os meus ossos.
5
Em volta de mim, ele construiu um muro de sofrimento e amargura.
6
Ele me fez morar na escuridão, como se eu estivesse morto há muito tempo.
7
Deus me amarrou com pesadas correntes; estou na prisão e não posso escapar.
8
Grito pedindo socorro, mas ele não quer ouvir a minha oração.
9
Não posso seguir em frente, pois, com grandes blocos de pedra, ele fechou o meu caminho.
10
Deus tem sido para mim como um leão de tocaia, como um urso pronto para atacar.
11
Ele me afastou do caminho, me fez em pedaços e depois me abandonou.
12
Ele armou o seu arco e fez de mim o alvo das suas flechas.
13
As flechas atiradas por Deus entraram fundo na minha carne.
14
O dia inteiro as pessoas riem de mim; elas zombam de mim nas suas canções.
15
Deus me encheu de comidas amargas e me fez beber fel até eu não poder mais.
16
Ele esfregou o meu rosto no chão e quebrou os meus dentes nas pedras.
17
Já não sei mais o que é paz e esqueci o que é felicidade.
18
Não tenho muito tempo de vida, e a minha esperança no SENHOR acabou.
19
Eu lembro da minha tristeza e solidão, das amarguras e dos sofrimentos.
20
Penso sempre nisso e fico abatido.
21
Mas a esperança volta quando penso no seguinte:
22
O amor do SENHOR Deus não se acaba, e a sua bondade não tem fim.
23
Esse amor e essa bondade são novos todas as manhãs; e como é grande a fidelidade do SENHOR!
24
Deus é tudo o que tenho; por isso, confio nele.
25
O SENHOR é bom para todos os que confiam nele.
26
O melhor é ter esperança e aguardar em silêncio a ajuda do SENHOR.
27
E é bom que as pessoas aprendam a sofrer com paciência desde a sua juventude.
28
Quando Deus nos faz sofrer, devemos ficar sozinhos, pacientes e em silêncio.
29
Devemos nos curvar, humildes, pois ainda pode haver esperança.
30
Quando somos ofendidos, não devemos reagir, mas sim suportar todos os insultos.
31
O Senhor não rejeita ninguém para sempre.
32
Ele pode fazer a gente sofrer, mas também tem compaixão porque o seu amor é imenso.
33
Não é com prazer que ele nos causa sofrimento ou dor.
34
Deus sabe quando neste país os prisioneiros são massacrados sem compaixão.
35
O Deus Altíssimo sabe quando são desrespeitados os direitos humanos, que ele mesmo nos deu.
36
Sim, o Senhor sabe quando torcem a justiça num processo.
37
Ninguém pode fazer acontecer nada se Deus não quiser.
38
Tanto as coisas boas como as más acontecem por ordem do Deus Altíssimo.
39
Por que nos queixarmos da vida quando somos castigados por causa dos nossos pecados?
40
Examinemos seriamente o que temos feito e voltemos para o SENHOR.
41
Abramos o nosso coração a Deus, que está no céu, e oremos assim:
42
“Ó Deus, nós pecamos, nos revoltamos, e não nos perdoaste.
43
“Tu ficaste irado conosco, nos perseguiste, nos mataste sem dó nem piedade.
44
Tu te cercaste de nuvens para que as nossas orações não chegassem a ti.
45
Fizeste com que as nações olhassem para nós como se fôssemos um monte de lixo e refugos.
46
“Somos insultados por todos os nossos inimigos.
47
Temos vivido no meio de medos, perigos, desgraças e destruição.
48
Dos meus olhos correm rios de lágrimas por causa da destruição do meu povo.
49
“Sem parar, os meus olhos vão derramar lágrimas
50
até que o SENHOR olhe lá do céu e nos veja.
51
O meu coração sofre muito quando penso no que vi acontecer com as mulheres da minha cidade.
52
“Os meus inimigos, que não tinham razão para me odiar, me caçaram como se eu fosse um passarinho.
53
Eles me jogaram vivo num poço e o taparam com uma pedra.
54
A água subiu acima da minha cabeça, e eu pensei: ‘Estou perdido! ’
55
“Do fundo do poço, gritei pedindo a tua ajuda, ó SENHOR.
56
Roguei que me escutasses, e tu ouviste o meu grito.
57
No dia em que te chamei, chegaste perto de mim e disseste: ‘Não tenha medo! ’
58
“Ó Senhor, tu vieste me socorrer e salvaste a minha vida.
59
Julga a meu favor, ó SENHOR, pois conheces as injustiças que tenho sofrido.
60
Tu sabes como os meus inimigos são vingativos e conheces os planos que fazem contra mim.
61
“Ó SENHOR Deus, tu ouviste os seus insultos e conheces todos os seus planos.
62
Tu sabes que o dia inteiro falam contra mim e planejam me prejudicar.
63
Tu vês que, em todos os momentos, eles zombam de mim.
64
“Ó SENHOR, dá-lhes o que merecem, castiga-os pelo que têm feito.
65
Amaldiçoa-os e faze com que eles caiam no desespero.
66
Persegue-os na tua ira, ó SENHOR, e acaba com eles aqui na terra! ”
Lamentações 3 - Nova Tradução na Linguagem de Hoje - NTLH
